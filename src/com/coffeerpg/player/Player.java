package com.coffeerpg.player;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import com.coffeerpg.engine.GraphicsLoader;
import com.coffeerpg.engine.Main;
import com.coffeerpg.map.GameMap;
import com.coffeerpg.tiles.Tile;

public class Player implements KeyListener, MouseListener {

	private int x, y;
	public static int width = 26, height = 26;

	private static final int Y_OFFSET = 21;
	private static final int X_OFFSET = 7;
	
	private static final int Y_SHADOW_OFFSET = 0;

	private int mX;
	private int mY;

	private String direction = "S";

	private boolean stepOne = true;
	private boolean stepTwo = false;
	private boolean stepThree = false;
	
	private boolean hasSword = false;

	boolean showMessage = false;
	private String playerString = "x:" + x + " y:" + y + " mX:" + mX + " mY:" + mY;

	//private Point currentTile;

	public Player(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}

	public void setDirection(String dir) {
		direction = dir;
	}

	public void nextStep() {
		if (stepOne) {
			stepOne = false;
			stepTwo = true;
			stepThree = false;
		} else if (stepTwo) {
			if (direction.equals("N") || direction.equals("S")) {
				stepOne = true;
				stepTwo = false;
				stepThree = false;
			}
			else {
				stepOne = false;
				stepTwo = false;
				stepThree = true;
			}
		} else if (stepThree) {
			stepOne = true;
			stepTwo = false;
			stepThree = true;
		}
	}

	public void update() {
		if (moveToClick) {
			if (x != (mX - width / 2)) {
				if (x < (mX - width / 2)) x++;
				if (x > (mX - width / 2)) x--;
			}
			if (y != (mY - height / 2)) {
				if (y < (mY - height / 2)) y++;
				if (y > (mY - height / 2)) y--;
			}
			if (x == (mX - width / 2) && y == (mY - height / 2)) moveToClick = false;
		}

		if (x > Main.WINDOW_WIDTH + 1) x = -width;
		else if (x < -width - 1) x = Main.WINDOW_WIDTH;

		if (y > Main.WINDOW_HEIGHT + 1) y = -height;
		else if (y < -height - 1) y = Main.WINDOW_HEIGHT;
	}

	public void paint(Graphics g) {
		if (direction.equals("N") || direction.equals("S")) g.drawImage(GraphicsLoader.tile_playerFrontBackShadow, x + X_OFFSET, y + Y_SHADOW_OFFSET, null);
		else if (direction.equals("E")) g.drawImage(GraphicsLoader.tile_playerSideShadow, x + X_OFFSET, y + Y_SHADOW_OFFSET, null);
		else g.drawImage(GraphicsLoader.tile_playerSideShadow, x + (X_OFFSET - 4), y + Y_SHADOW_OFFSET, null);
		
		if (stepOne) {
			if (direction.equals("N")) g.drawImage(GraphicsLoader.tile_playerBackStep1, x + X_OFFSET, y - Y_OFFSET, null);
			if (direction.equals("S")) g.drawImage(GraphicsLoader.tile_playerFrontStep1, x + X_OFFSET, y - Y_OFFSET, null);
			if (direction.equals("E")) g.drawImage(GraphicsLoader.tile_playerRightStep1, x + X_OFFSET, y - Y_OFFSET, null);
			if (direction.equals("W")) g.drawImage(GraphicsLoader.tile_playerLeftStep1, x + X_OFFSET, y - Y_OFFSET, null);

		} else if (stepTwo) {
			if (direction.equals("N")) g.drawImage(GraphicsLoader.tile_playerBackStep2, x + X_OFFSET, y - Y_OFFSET, null);
			if (direction.equals("S")) g.drawImage(GraphicsLoader.tile_playerFrontStep2, x + X_OFFSET, y - Y_OFFSET, null);
			if (direction.equals("E")) g.drawImage(GraphicsLoader.tile_playerRightStep2, x + X_OFFSET, y - Y_OFFSET, null);
			if (direction.equals("W")) g.drawImage(GraphicsLoader.tile_playerLeftStep2, x + X_OFFSET, y - Y_OFFSET, null);
		} else if (stepThree) {
			if (direction.equals("N")) g.drawImage(GraphicsLoader.tile_playerBackStep2, x + X_OFFSET, y - Y_OFFSET, null);
			if (direction.equals("S")) g.drawImage(GraphicsLoader.tile_playerFrontStep2, x + X_OFFSET, y - Y_OFFSET, null);
			if (direction.equals("E")) g.drawImage(GraphicsLoader.tile_playerRightStep3, x + X_OFFSET, y - Y_OFFSET, null);
			if (direction.equals("W")) g.drawImage(GraphicsLoader.tile_playerLeftStep3, x + X_OFFSET, y - Y_OFFSET, null);
		}

		
		
		if (showMessage) {
			FontMetrics metrics = g.getFontMetrics();
			playerString = "x:" + x + " y:" + y;
			g.drawString(playerString, (x + (width / 2) - (metrics.stringWidth(playerString) / 2)), y - 25);
		}
	}

	// Input handlers
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {

		case KeyEvent.VK_LEFT:
			setDirection("W");
			boolean collides = false;
			for (Tile t : GameMap.collidableTiles) {
				Rectangle r = new Rectangle(x - 30, y, 30, 30);
				if (r.intersects(t.getBounds())) {
					collides = true;
				} 
			}
			if (!collides) { x-=30; nextStep(); }
			break;
		case KeyEvent.VK_RIGHT:
			setDirection("E");
			collides = false;
			for (Tile t : GameMap.collidableTiles) {
				Rectangle r = new Rectangle(x + 30, y, 30, 30);
				if (r.intersects(t.getBounds())) {
					collides = true;
				} 
			}
			if (!collides) { x+=30; nextStep(); }
			break;
		case KeyEvent.VK_UP:
			setDirection("N");
			collides = false;
			for (Tile t : GameMap.collidableTiles) {
				Rectangle r = new Rectangle(x, y - 30, 30, 30);
				if (r.intersects(t.getBounds())) {
					collides = true;
				} 
			}
			if (!collides) { y-=30; nextStep(); }
			break;
		case KeyEvent.VK_DOWN:
			setDirection("S");
			collides = false;
			for (Tile t : GameMap.collidableTiles) {
				Rectangle r = new Rectangle(x, y + 30, 30, 30);
				if (r.intersects(t.getBounds())) {
					collides = true;
				} 
			}
			if (!collides) { y+=30; nextStep(); }
			break;
		case KeyEvent.VK_ENTER:
			showMessage = true;
			break;
		case KeyEvent.VK_BACK_SPACE:
			showMessage = false;
			break;
		case KeyEvent.VK_SPACE:
			if (hasSword) //attack();
			break;

		}
	}

	public void keyReleased(KeyEvent e) {
	}

	public void keyTyped(KeyEvent e) {
	}

	boolean moveToClick = false;

	@Override
	public void mouseClicked(MouseEvent e) {
		moveToClick = true;
		mX = e.getX();
		mY = e.getY();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
