package com.coffeerpg.npc;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Random;

import com.coffeerpg.engine.GraphicsLoader;

public class NPC {

	private int x, y;
	public static int width = 26, height = 26;

	boolean showMessage = false;

	/** movement variables */
	private Random randInt;
	private int counter = 0;
	private int coolOffPeriod = 140000;
	private boolean headSouth = false;

	/** chat messages */
	private String[] chatMsg = { "Hi!", "I'm NPC!" };
	private int chatCoolOff = 500000;
	private int chatCounter = 0;

	private String currentMessage = "";

	public NPC(int x, int y) {
		this.x = x;
		this.y = y;

		randInt = new Random();
	}

	public void update() {
		if (counter <= coolOffPeriod) {
			counter++;
		} else {
			counter = 0;
			if (headSouth) {

				if (randInt.nextBoolean()) {
					y += 30;
					headSouth = false;
				}
				counter = 0;

			} else {

				if (randInt.nextBoolean()) {
					y -= 30;
					headSouth = true;
				}
				counter = 0;
			}
		}

		/** chat stuff */
		if (chatCounter < chatCoolOff) {
			chatCounter++;
			if (chatCounter == (chatCoolOff / 3) * 2) {
				currentMessage = "";
			}
			// System.out.println(chatCounter);
		} else {
			chatCounter = 0;
			if (randInt.nextBoolean()) {
				currentMessage = chatMsg[randInt.nextInt(3)];
			} else {
				currentMessage = "";
			}
		}

	}

	public void paint(Graphics g) {
		update();
		g.drawImage(GraphicsLoader.tile_npc_wizard, x, y, null);
		//g.drawImage(GraphicsLoader.tile_npc_shadow, x, y, null);

		g.setColor(Color.WHITE);
		FontMetrics metrics = g.getFontMetrics();
		g.drawString(
				currentMessage,
				((x) + (width / 2) - (metrics.stringWidth(currentMessage) / 2)),
				(y) - 5);
	}

	public Point getCurrentTile() {
		int varx = x / 30;
		int vary = y / 30;

		return (new Point(varx, vary + 1));
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}

}
