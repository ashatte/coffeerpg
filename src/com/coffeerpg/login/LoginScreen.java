package com.coffeerpg.login;

import java.applet.Applet;
import java.awt.Button;
import java.awt.TextField;

public class LoginScreen extends Applet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Button bttnSubmit;
	TextField tfUsername, tfPassword;
	
	public void init() {
		bttnSubmit = new Button("Login");
		
		tfUsername = new TextField("Username", 10);
		tfPassword = new TextField("Password", 10);
		
		add(tfUsername);
		add(tfPassword);
		add(bttnSubmit);
	}

}
