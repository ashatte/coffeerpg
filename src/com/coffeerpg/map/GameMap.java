package com.coffeerpg.map;

import java.awt.Graphics;
import java.util.ArrayList;

import com.coffeerpg.engine.GraphicsLoader;
import com.coffeerpg.engine.Main;
import com.coffeerpg.tiles.GrassOverlapTile;
import com.coffeerpg.tiles.GrassTile;
import com.coffeerpg.tiles.SandTile;
import com.coffeerpg.tiles.Tile;
import com.coffeerpg.tiles.TreeTile;
import com.coffeerpg.tiles.WaterTile;

public class GameMap implements TileBasedMap {

	/** A reference to the main applet class */
	private Main m;

	/** The map width in tiles */
	public static final int WIDTH = 50;
	/** The map height in tiles */
	public static final int HEIGHT = 50;

	/** Tile size **/
	public static final int TILE_SIZE = 30;

	/** Indicate grass terrain at a given location */
	public static final int GRASS = 0;
	public static final int GRASSTL = 1;
	public static final int GRASSTC = 2;
	public static final int GRASSTR = 3;
	public static final int GRASSML = 4;
	public static final int GRASSMR = 5;
	public static final int GRASSBL = 6;
	public static final int GRASSBC = 7;
	public static final int GRASSBR = 8;
	/** Indicate water terrain at a given location */
	public static final int WATER = 9;
	/** Indicate trees terrain at a given location */
	public static final int TREE = 10;
	/** Indicate sand terrain at a given location */
	public static final int SAND = 11;
	public static final int SANDTL = 12;
	public static final int SANDTC = 13;
	public static final int SANDTR = 14;
	public static final int SANDML = 15;
	public static final int SANDMR = 16;
	public static final int SANDBL = 17;
	public static final int SANDBC = 18;
	public static final int SANDBR = 19;

	/** The terrain settings for each tile in the map */
	private Tile[][] terrain = new Tile[WIDTH][HEIGHT];
	/** The unit in each tile of the map */
	// private int[][] units = new int[WIDTH][HEIGHT];

	/** overlaps **/
	private Tile[][] overlaps_top = new Tile[WIDTH][HEIGHT];
	private Tile[][] overlaps_right = new Tile[WIDTH][HEIGHT];
	private Tile[][] overlaps_bottom = new Tile[WIDTH][HEIGHT];
	private Tile[][] overlaps_left = new Tile[WIDTH][HEIGHT];
	
	public static ArrayList<Tile> collidableTiles = new ArrayList<Tile>();

	public GameMap(Main m) {
		this.m = m;

		
		loadTilesFromPixels();

		generateOverlaps();
	}

	// iterate through pixels of map image and load map based on color
	private void loadTilesFromPixels() {
		for (int y = 0; y < HEIGHT; y++) {
			for (int x = 0; x < WIDTH; x++) {
				int clr = GraphicsLoader.img_worldmap.getRGB(x, y);
				int red = (clr & 0x00ff0000) >> 16;
				int green = (clr & 0x0000ff00) >> 8;
				int blue = clr & 0x000000ff;
				/** SAND TILES **/
				if (red == 255)
					insertTile(x, y, SAND);
				/** TREE FIRST PARSE GRASS **/
				// else if (green == 100) insertTile(x, y, GRASS);
				/** GRASS TILES **/
				else if (green == 255 || green == 100)
					insertTile(x, y, GRASS);
				else if (green == 254)
					insertTile(x, y, GRASSTL);
				else if (green == 253)
					insertTile(x, y, GRASSTC);
				else if (green == 252)
					insertTile(x, y, GRASSTR);
				else if (green == 251)
					insertTile(x, y, GRASSMR);
				else if (green == 250)
					insertTile(x, y, GRASSBR);
				else if (green == 249)
					insertTile(x, y, GRASSBC);
				else if (green == 248)
					insertTile(x, y, GRASSBL);
				else if (green == 247)
					insertTile(x, y, GRASSML);
				else if (blue == 255)
					insertTile(x, y, WATER);

				/** TREE **/
				if (green == 100)
					insertTile(x, y, TREE);
			}
		} 
	}

	public void update(Graphics g) {
		draw(g);
	}

	private void draw(Graphics g) {

		for (int x = 0; x < WIDTH; x++) {
			for (int y = 0; y < HEIGHT; y++) {

				if (terrain[x][y].getTileType() == "GRASS" || terrain[x][y].getTileType() == "WATER") {
					terrain[x][y].draw(g);
				}
			}
		}

		for (int x = 0; x < WIDTH; x++) {
			for (int y = 0; y < HEIGHT; y++) {
				if (overlaps_top[x][y] != null)
					overlaps_top[x][y].draw(g);
				if (overlaps_bottom[x][y] != null)
					overlaps_bottom[x][y].draw(g);
				if (overlaps_left[x][y] != null)
					overlaps_left[x][y].draw(g);
				if (overlaps_right[x][y] != null)
					overlaps_right[x][y].draw(g);
				if (terrain[x][y].getTileType() == "SAND")
					terrain[x][y].draw(g);
				Main.npc.paint(g);
				if (terrain[x][y].getTileType() == "TREE") {
					terrain[x][y].draw(g);
				}
			}
		}

		for (int x = 0; x < WIDTH; x++) {
			for (int y = 0; y < HEIGHT; y++) {
				if (terrain[x][y].getTileType() == "TREE") {
					terrain[x][y].draw(g);
				}
				if (Main.p.getX() >= (x * 30) && Main.p.getX() <= (x*30)+30 && Main.p.getY() >= (y*30) && Main.p.getY() <= (y*30)+30)
					Main.p.paint(g);
			}
		}
	}

	
	// insert a tile of type into terrain array
	private void insertTile(int row, int col, int type) {
		switch (type) {
		case GRASS:
			terrain[row][col] = new GrassTile(row * TILE_SIZE, col * TILE_SIZE, type);
			break;
		case GRASSTL:
			terrain[row][col] = new GrassTile(row * TILE_SIZE, col * TILE_SIZE, type);
			break;
		case GRASSTC:
			terrain[row][col] = new GrassTile(row * TILE_SIZE, col * TILE_SIZE, type);
			break;
		case GRASSTR:
			terrain[row][col] = new GrassTile(row * TILE_SIZE, col * TILE_SIZE, type);
			break;
		case GRASSML:
			terrain[row][col] = new GrassTile(row * TILE_SIZE, col * TILE_SIZE, type);
			break;
		case GRASSMR:
			terrain[row][col] = new GrassTile(row * TILE_SIZE, col * TILE_SIZE, type);
			break;
		case GRASSBL:
			terrain[row][col] = new GrassTile(row * TILE_SIZE, col * TILE_SIZE, type);
			break;
		case GRASSBC:
			terrain[row][col] = new GrassTile(row * TILE_SIZE, col * TILE_SIZE, type);
			break;
		case GRASSBR:
			terrain[row][col] = new GrassTile(row * TILE_SIZE, col * TILE_SIZE, type);
			break;
		case WATER:
			terrain[row][col] = new WaterTile(m, row * TILE_SIZE, col * TILE_SIZE);
			break;
		case TREE:
			terrain[row][col] = new TreeTile(m, row * TILE_SIZE, col * TILE_SIZE);
			break;
		case SAND:
			terrain[row][col] = new SandTile(m, row * TILE_SIZE, col * TILE_SIZE);
			break;
		}

	}

	// generate grass edges over water
	private void generateOverlaps() {
		for (int x = 0; x < WIDTH; x++) {
			for (int y = 0; y < HEIGHT; y++) {
				if (terrain[x][y].getTileType() == "WATER") {
					/* left tile */
					if (x > 1 && x < WIDTH - 1 && y > 1 && y < HEIGHT - 1) {
						/* left tile */
						if (terrain[x - 1][y] != null && (terrain[x - 1][y].getTileType() == "GRASS" || terrain[x - 1][y].getTileType() == "TREE"))
							overlaps_left[x - 1][y] = new GrassOverlapTile(x * TILE_SIZE, y * TILE_SIZE, 3);
						/* right tile */
						if (terrain[x + 1][y] != null && (terrain[x + 1][y].getTileType() == "GRASS" || terrain[x + 1][y].getTileType() == "TREE"))
							overlaps_right[x - 1][y] = new GrassOverlapTile(x * TILE_SIZE, y * TILE_SIZE, 1);
						/* top tile */
						if (terrain[x][y - 1] != null && (terrain[x][y - 1].getTileType() == "GRASS" || terrain[x][y - 1].getTileType() == "TREE"))
							overlaps_top[x - 1][y] = new GrassOverlapTile(x * TILE_SIZE, y * TILE_SIZE, 0);
						/* bottom tile ( didn't do trees for obvious reasons ) */
						if (terrain[x][y + 1] != null && (terrain[x][y + 1].getTileType() == "GRASS"))
							overlaps_bottom[x - 1][y] = new GrassOverlapTile(x * TILE_SIZE, y * TILE_SIZE, 2);
					}
				}
			}
		}
	}

	@Override
	public int getWidthInTiles() {
		return WIDTH * TILE_SIZE;
	}

	@Override
	public int getHeightInTiles() {
		return HEIGHT * TILE_SIZE;
	}

}
