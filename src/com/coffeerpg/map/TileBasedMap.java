package com.coffeerpg.map;

// interface - plan to have similar pixel-based maps to load overground features (buildings etc.)
public interface TileBasedMap {
	
	public int getWidthInTiles();
	
	public int getHeightInTiles();

}
