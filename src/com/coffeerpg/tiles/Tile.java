package com.coffeerpg.tiles;

import java.awt.Graphics;
import java.awt.Rectangle;

public interface Tile {
	boolean isCollidable = false;
	public void draw(Graphics g);
	public String getTileType();
	public boolean isCollidable();
	public Rectangle getBounds();
}
