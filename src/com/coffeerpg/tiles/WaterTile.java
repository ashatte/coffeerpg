package com.coffeerpg.tiles;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Random;

import com.coffeerpg.engine.GraphicsLoader;
import com.coffeerpg.engine.Main;
import com.coffeerpg.map.GameMap;

public class WaterTile implements Tile {

	/* reference to sprite sheet position */
	private int spritesheet_index;

	/* sprite sheet values */
	final int width = 30;
	final int height = 30;
	final int rows = 4;
	final int cols = 4;

	/* value to hold split up sprites */
	BufferedImage[] sprites = new BufferedImage[rows * cols];

	private static final String TILE_TYPE = "WATER";

	private int x, y;

	/* Random number */
	private Random randInt;

	/* Draw counter */
	private int counter = 0;
	private int delay = 30;

	public WaterTile(Main m, int x, int y) {
		this.x = x;
		this.y = y;

		randInt = new Random();
		delay = 40;
		this.spritesheet_index = randInt.nextInt(9);

		// split the sprite sheet into sprites */
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				sprites[(i * cols) + j] = GraphicsLoader.tilesheet_water.getSubimage(j * width, i * height, width, height);
			}
		}

		GameMap.collidableTiles.add(this);
		// this.TILE_SPRITE = Toolkit.getDefaultToolkit().getImage("assets/tiles/tile_water.jpg");
	}

	public void draw(Graphics g) {
		if (counter == 0 || counter == 1) {
			spritesheet_index = randInt.nextInt(9);
			counter++;
		} else {
			if (counter < delay) counter++;
			else
				counter = 0;
		}
		// if (spritesheet_index > 8) spritesheet_index = 0;
		g.drawImage(sprites[spritesheet_index], x, y, null);
		counter++;

	}

	@Override
	public String getTileType() {
		return TILE_TYPE;
	}

	@Override
	public boolean isCollidable() {
		return true;
	}
	
	@Override
	public Rectangle getBounds() {
		return new Rectangle(x, y, 30, 30);
	}

}
