package com.coffeerpg.tiles;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Random;

import com.coffeerpg.engine.GraphicsLoader;
import com.coffeerpg.engine.Main;

public class GrassTile implements Tile {

	/* Sprite sheet positions */
	private static final int CC = 0;
	private static final int TL = 1;
	private static final int TC = 2;
	private static final int TR = 3;
	private static final int ML = 4;
	private static final int MR = 5;
	private static final int BL = 6;
	private static final int BC = 7;
	private static final int BR = 8;

	/* reference to sprite sheet position */
	private int spritesheet_index;

	/* sprite sheet values */
	final int width = 30;
	final int height = 30;
	final int rows = 3;
	final int cols = 3;
	
	Random r;
	int rotate = 0;

	/* value to hold split up sprites */
	BufferedImage[] sprites = new BufferedImage[rows * cols];

	private static final String TILE_TYPE = "GRASS";

	private int x, y;

	public GrassTile(int x, int y, int spritesheet_index) {
		this.x = x;
		this.y = y;
		
		r = new Random();
		rotate = r.nextInt(4);

		this.spritesheet_index = spritesheet_index;

		// split the sprite sheet into sprites */
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				sprites[(i * cols) + j] = GraphicsLoader.tilesheet_grass.getSubimage(j * width, i * height, width, height);
			}
		}
	}

	public void draw(Graphics g) {
		g.drawImage(getSSImage(spritesheet_index), x, y, null);
	}

	@Override
	public String getTileType() {
		return TILE_TYPE;
	}

	@Override
	public boolean isCollidable() {
		Point p = Main.npc.getCurrentTile();
		Point c = new Point((x / 30), (y / 30));
		System.out.println("c = " + c + " and p = " + p);
		if (p.x == c.x && p.y == c.y) {
			return true;
		} else {
			return false;
		}
	}

	private Image getSSImage(int ss_idx) {
		switch (ss_idx) {
		case CC:
			return sprites[4];
		case TL:
			return sprites[0];
		case TC:
			return sprites[1];
		case TR:
			return sprites[2];
		case ML:
			return sprites[3];
		case MR:
			return sprites[5];
		case BL:
			return sprites[6];
		case BC:
			return sprites[7];
		case BR:
			return sprites[8];
		}
		return null;
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(x, y, 30, 30);
	}


}
