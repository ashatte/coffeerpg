package com.coffeerpg.tiles;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.coffeerpg.engine.GraphicsLoader;

public class GrassOverlapTile implements Tile {
	
	private static final String TILE_TYPE = "GRASSOVERLAP";
	
	private int x, y;
	
	private int direction;
	
	private static final int OVERLAP_TOP = 0;
	private static final int OVERLAP_RIGHT = 1;
	private static final int OVERLAP_BOTTOM = 2;
	private static final int OVERLAP_LEFT = 3;

	public GrassOverlapTile(int x, int y, int direction) {
		this.x = x; 
		this.y = y;
		
		this.direction = direction;
	}
	
	public void draw(Graphics g) {
		g.drawImage(getOverlapImage(), x, y, null);
	}
	
	private Image getOverlapImage() {
		switch(direction) {
		case OVERLAP_TOP:
			return GraphicsLoader.overlap_grasst;
		case OVERLAP_RIGHT:
			return GraphicsLoader.overlap_grassr;
		case OVERLAP_BOTTOM:
			return GraphicsLoader.overlap_grassb;
		case OVERLAP_LEFT:
			return GraphicsLoader.overlap_grassl;
		}
		return null;
	}
	
	@Override
	public String getTileType() {
		return TILE_TYPE;
	}

	public boolean isCollidable() {
		return false;
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(x, y, 30, 30);
	}

}

