package com.coffeerpg.tiles;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

import com.coffeerpg.engine.GraphicsLoader;
import com.coffeerpg.engine.Main;
import com.coffeerpg.map.GameMap;

public class TreeTile implements Tile {

	private static final String TILE_TYPE = "TREE";

	private int x, y;

	private int HEIGHT_OFFSET = 50;
	private int WIDTH_OFFSET = 6;

	private Random random;
	private boolean drawState = false;

	private boolean firstPass = true;

	/* Draw counter */
	private int counter = 0;
	private final int delay;

	public TreeTile(Main m, int x, int y) {
		this.x = x;
		this.y = y;

		HEIGHT_OFFSET = HEIGHT_OFFSET - x % 9;
		WIDTH_OFFSET = WIDTH_OFFSET + y % 9;

		random = new Random();
		delay =  80 - random.nextInt(40) + 40;
		
		GameMap.collidableTiles.add(this);
	}

	@Override
	public void draw(Graphics g) {

		if (firstPass == true) {
			g.drawImage(GraphicsLoader.tile_grass_a, x, y, null);
			firstPass = false;

		} else {
			if (counter == 0 || counter == 1) { drawState = random.nextBoolean(); counter++; }
			else if (counter == delay) { counter = 0; }
			if (drawState == true) { g.drawImage(GraphicsLoader.tile_tree, (x) - WIDTH_OFFSET, (y) - HEIGHT_OFFSET, null); counter++; }
			else { g.drawImage(GraphicsLoader.tile_tree2, (x) - WIDTH_OFFSET, (y) - HEIGHT_OFFSET, null); counter++; }
			firstPass = true;

		}
	}

	@Override
	public String getTileType() {
		return TILE_TYPE;
	}

	@Override
	public boolean isCollidable() {
		return true;
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(x, y, 30, 30);
	}

}
