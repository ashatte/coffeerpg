package com.coffeerpg.tiles;

import java.awt.Graphics;
import java.awt.Rectangle;

import com.coffeerpg.engine.GraphicsLoader;
import com.coffeerpg.engine.Main;

public class SandTile implements Tile {
	
	private static final String TILE_TYPE = "SAND";
	
	private int x, y;
	
	private int HEIGHT_OFFSET = 0;
	private int WIDTH_OFFSET = 0;

	public SandTile(Main m, int x, int y) {
		this.x = x; 
		this.y = y;
		
		HEIGHT_OFFSET = HEIGHT_OFFSET - x % 7;
		WIDTH_OFFSET = WIDTH_OFFSET - y % 9;
		//this.TILE_SPRITE = Toolkit.getDefaultToolkit().getImage("assets/tiles/tile_water.jpg");
	}
	
	public void draw(Graphics g, int oX, int oY) {
		g.drawImage(GraphicsLoader.tile_sand_a, (x-3 + oX), (y-3 + oY), null);
		g.drawImage(GraphicsLoader.tile_sand_a, (x-3 + oX) - WIDTH_OFFSET, (y-3 + oY) - HEIGHT_OFFSET, null);
	}
	
	@Override
	public String getTileType() {
		return TILE_TYPE;
	}

	public boolean isCollidable() {
		return false;
	}

	@Override
	public void draw(Graphics g) {
		g.drawImage(GraphicsLoader.tile_sand_a, (x-3), (y-3), null);
		g.drawImage(GraphicsLoader.tile_sand_a, (x-3) - WIDTH_OFFSET, (y-3) - HEIGHT_OFFSET, null);
	}
	
	@Override
	public Rectangle getBounds() {
		return new Rectangle(x, y, 30, 30);
	}

}
