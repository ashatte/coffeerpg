package com.coffeerpg.engine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class NetworkClient {
	
	private String serverAddress = "localhost";
	private int port = 2323;
	Socket s;
	
	BufferedReader input;
	
	public NetworkClient() throws Exception {
		s = new Socket(serverAddress, port);
		
		input = new BufferedReader(new InputStreamReader(s.getInputStream()));
	}
	
	public void printServerOutput() throws IOException {
		System.out.println(input.readLine());
	}

}
