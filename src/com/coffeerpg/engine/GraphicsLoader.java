package com.coffeerpg.engine;

import java.applet.Applet;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.imageio.ImageIO;

public class GraphicsLoader {
	
	private Applet context;
	
	/** GRAPHICS */
	
	/** map */
	public static BufferedImage img_worldmap;
	
	/** player tiles */
	public static Image tile_playerFront;
	public static Image tile_playerFrontStep1;
	public static Image tile_playerFrontStep2;
	public static Image tile_playerBack;
	public static Image tile_playerBackStep1;
	public static Image tile_playerBackStep2;
	public static Image tile_playerLeft;
	public static Image tile_playerLeftStep1;
	public static Image tile_playerLeftStep2;
	public static Image tile_playerLeftStep3;
	public static Image tile_playerRight;
	public static Image tile_playerRightStep1;
	public static Image tile_playerRightStep2;
	public static Image tile_playerRightStep3;

	/** player shadow */
	public static Image tile_playerFrontBackShadow;
	public static Image tile_playerSideShadow;
	
	/** npc */
	public static Image tile_npc_wizard;
	public static Image tile_npc_shadow;

	/** tiles & tilesheets */
	public static BufferedImage tilesheet_grass;
	public static BufferedImage tilesheet_water;
	public static Image tile_grass;
	public static Image tile_grass_a;
	public static Image tile_water;
	public static Image tile_water_a;
	public static Image tile_sand;
	public static Image tile_sand_a;
	public static Image tile_tree;
	public static Image tile_tree2;

	/** tile overlaps */
	public static Image overlap_grasst;
	public static Image overlap_grassr;
	public static Image overlap_grassb;
	public static Image overlap_grassl;
	
	public GraphicsLoader(Applet a) {
		context = a;
		
		try {
			loadGraphics();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void loadGraphics() throws MalformedURLException, IOException {
	
		/* init the world map image */
		URL url = new URL(context.getCodeBase(), "assets/map/worldmap.png");
		img_worldmap = ImageIO.read(url);

		/* map tilesheets */
		url = new URL(context.getCodeBase(),"assets/tiles/tilesheet_grasspix.png");
		tilesheet_grass = ImageIO.read(url);
		url = new URL(context.getCodeBase(), "assets/tiles/tilesheet_water2.png");
		tilesheet_water = ImageIO.read(url);


		/* map tiles */
		tile_water = context.getImage(context.getCodeBase(), "assets/tiles/tile_water.png");
		tile_water_a = context.getImage(context.getCodeBase(), "assets/tiles/tile_water.png");
		tile_grass = context.getImage(context.getCodeBase(), "assets/tiles/tile_grass2.png");
		tile_grass_a = context.getImage(context.getCodeBase(), "assets/tiles/tile_grass.png");
		tile_sand = context.getImage(context.getCodeBase(), "assets/tiles/tile_dirt.png");
		tile_sand_a = context.getImage(context.getCodeBase(), "assets/tiles/tile_sand_a.png");
		tile_tree = context.getImage(context.getCodeBase(), "assets/tiles/tile_tree.png");
		tile_tree2 = context.getImage(context.getCodeBase(), "assets/tiles/tile_tree2.png");

		/* overlap tiles */
		overlap_grasst = context.getImage(context.getCodeBase(),"assets/tiles/overlap_grasst.png");
		overlap_grassr = context.getImage(context.getCodeBase(),"assets/tiles/overlap_grassr.png");
		overlap_grassb = context.getImage(context.getCodeBase(),"assets/tiles/overlap_grassb.png");
		overlap_grassl = context.getImage(context.getCodeBase(),"assets/tiles/overlap_grassl.png");

		/* player graphics */
		tile_playerFront = context.getImage(context.getCodeBase(),"assets/player/front26.png");
		tile_playerFrontStep1 = context.getImage(context.getCodeBase(),"assets/player/front_step1.png");
		tile_playerFrontStep2 = context.getImage(context.getCodeBase(),"assets/player/front_step2.png");
		tile_playerBack = context.getImage(context.getCodeBase(),"assets/player/back.png");
		tile_playerBackStep1 = context.getImage(context.getCodeBase(),"assets/player/back-step1.png");
		tile_playerBackStep2 = context.getImage(context.getCodeBase(),"assets/player/back-step2.png");
		tile_playerLeft = context.getImage(context.getCodeBase(),"assets/player/left26.png");
		tile_playerLeftStep1 = context.getImage(context.getCodeBase(),"assets/player/left_step1.png");
		tile_playerLeftStep2 = context.getImage(context.getCodeBase(),"assets/player/left_step2.png");
		tile_playerLeftStep3 = context.getImage(context.getCodeBase(),"assets/player/left_step3.png");
		tile_playerRight = context.getImage(context.getCodeBase(),"assets/player/right26.png");
		tile_playerRightStep1 = context.getImage(context.getCodeBase(),"assets/player/right_step1.png");
		tile_playerRightStep2 = context.getImage(context.getCodeBase(),"assets/player/right_step2.png");
		tile_playerRightStep3 = context.getImage(context.getCodeBase(),"assets/player/right_step3.png");
		tile_playerFrontBackShadow = context.getImage(context.getCodeBase(),"assets/player/shadow_front.png");
		tile_playerSideShadow = context.getImage(context.getCodeBase(),"assets/player/shadowside.png");
		
		/** npc graphics */
		tile_npc_wizard = context.getImage(context.getCodeBase(),"assets/player/wizard.png");
		tile_npc_shadow = context.getImage(context.getCodeBase(),"assets/player/wizard_shadow.png");
	
	}

}
