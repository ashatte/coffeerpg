package com.coffeerpg.audio;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;

public class MusicPlayer {

	private Sequence sequence;
	private Sequencer sequencer;
	
	private boolean mute = true;
	
	private static final String BEGINNING_SONG = "corymbia.mid";

	public MusicPlayer() {
		loadAndPlayFile(BEGINNING_SONG);
	}

	private void loadAndPlayFile(String songName) {
		try {
			// From file
			sequence = MidiSystem.getSequence(new File("assets/music/".concat(songName)));

			// Create a sequencer for the sequence
			sequencer = MidiSystem.getSequencer();
			//receiver = MidiSystem.getReceiver();
			sequencer.open();
			sequencer.setSequence(sequence);

			sequencer.setLoopCount(Sequencer.LOOP_CONTINUOUSLY);
			// Start playing
			sequencer.start();

		} catch (MalformedURLException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		} catch (MidiUnavailableException e) {
			System.out.println(e);
		} catch (InvalidMidiDataException e) {
			System.out.println(e);
		}

	}

	public void setMute() {
		if (mute) {
			sequencer.stop();
			mute = false;
		} else {
			sequencer.start();
			mute = true;
		}
	}

}
