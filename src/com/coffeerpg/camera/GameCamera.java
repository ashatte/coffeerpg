package com.coffeerpg.camera;

import com.coffeerpg.player.Player;

public class GameCamera {
	
	private int VIEWPORT_SIZE_X = 600, VIEWPORT_SIZE_Y = 500;
	
	private int WORLD_SIZE_X = 1500, WORLD_SIZE_Y = 1500;
	
	private int offsetMaxY, offsetMinY, offsetMinX, offsetMaxX;
	
	private int camX, camY;
	
	public int getCamX() {
		return camX;
	}

	public void setCamX(int camX) {
		this.camX = camX;
	}

	public int getCamY() {
		return camY;
	}

	public void setCamY(int camY) {
		this.camY = camY;
	}

	public GameCamera(Player p) {
		offsetMaxX = WORLD_SIZE_X - VIEWPORT_SIZE_X;
		offsetMaxY = WORLD_SIZE_Y - VIEWPORT_SIZE_Y;
		offsetMinX = 0;
		offsetMinY = 0;
	}
	
	public void centerCamera(Player p) {

		camX = p.getX() - VIEWPORT_SIZE_X / 2;
		camY = p.getY() - VIEWPORT_SIZE_Y / 2;
		
		if (camX > offsetMaxX) setCamX(offsetMaxX);
		else if (camX < offsetMinX) setCamX(offsetMinX);
		
		if (camY > offsetMaxY) setCamY(offsetMaxY); 
		else if (camY < offsetMinY) setCamY(offsetMinY);
	} 
	

}
