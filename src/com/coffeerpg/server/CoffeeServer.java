package com.coffeerpg.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;

public class CoffeeServer extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JButton bttnStart, bttnStop;
	private JTextArea txtMessages;
	
	
	public CoffeeServer() {
		
		bttnStart = new JButton("start");
		bttnStop = new JButton("stop");
		
		txtMessages = new JTextArea(10, 8);
		txtMessages.setEditable(false);
		
		add(bttnStart);
		add(bttnStop);
		
		add(txtMessages);
		
		setSize(600, 600);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		
	}

	public static void main(String[] args) throws IOException {
		new CoffeeServer();
		System.out.println("Server running on port 2323...");
		ServerSocket listener = new ServerSocket(2323);
		Random r = new Random();
		try {
			while (true) {
				Socket socket = listener.accept();
				try {
					PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
					out.println(r.nextBoolean());
				} finally {
					socket.close();
				}
			}
		} finally {
			listener.close();
		}
	}
}
